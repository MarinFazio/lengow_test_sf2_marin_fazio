<?php

namespace Test\TestBundle\Controller;

use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Test\TestBundle\Entity\MyOrder;
use Test\TestBundle\Form\MyOrderType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $lengow_test = $this->get('lengow_test');
        $flux_xml = $lengow_test->readXML($this->container->getParameter('lengow_test.url_orders'));

        $crawler = new Crawler($flux_xml);

        $orders = $crawler->filter('order');
        $em = $this->getDoctrine()->getManager();
        $order_respository = $em->getRepository('TestBundle:MyOrder');

        foreach ($orders as $order) {
            $persist = true;
            $myorder = new MyOrder();
            foreach($order->childNodes as $child){
                if($child->tagName == "order_id"){
                    if($order_respository->findOneBy(array('orderId' => $child->nodeValue))!=null){
                        $persist = false;
                    }
                }
                switch($child->tagName){
                    case "order_id":
                        $myorder->setOrderId($child->nodeValue);
                        break;
                    case "marketplace":
                        $myorder->setMarketplace($child->nodeValue);
                        break;
                    case "billing_address":
                        $myorder->setBillingAddress($child->nodeValue);
                        break;
                    case "delivery_address":
                        $myorder->setDeliveryAddress($child->nodeValue);
                        break;
                    default:
                        break;
                }
            }
            if($persist == true){
                $em->persist($myorder);
            }
        }
        $em->flush();

        $source = new Entity("TestBundle:MyOrder");
        $grid = $this->get('grid');
        $grid->setSource($source);

        return $grid->getGridResponse("TestBundle:Default:index.html.twig");

//        return $this->render('TestBundle:Default:index.html.twig');
    }

    public function createAction(Request $request)
    {
        $my_order = new MyOrder();
        $form = $this->get('form.factory')->create(new MyOrderType(), $my_order);

        if($form->handleRequest($request)->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($my_order);
            $em->flush();

            return $this->redirect($this->generateUrl('test_homepage'));
        }
        return $this->render('TestBundle:Default:create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function getJSONAction($id_order=0, $yml="")
    {
        $serializer = $this->container->get('jms_serializer');

        $em = $this->getDoctrine()->getManager();
        $my_order_respository = $em->getRepository('TestBundle:MyOrder');

        if($yml!="yml"){
            $format='json';
        } else{
            $format='yml';
        }

        if($id_order==0){
            $data = $my_order_respository->findAll();
        } else{
            $data = $my_order_respository->findOneBy(array('orderId' => $id_order));
        }

        $output = $serializer->serialize($data, $format);

        return $this->render('TestBundle:Default:getoutput.html.twig', array(
            'output' => $output,
        ));
    }

}
